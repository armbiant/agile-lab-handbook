# Library

In AgileLab, we have some technical books, both physical and digital.

## Physical libraries
We have some physical books in company offices. 

You can find the list of available physical books in `sharepoint://Big Data/Documents/Library/Libreria Agile.xlsx`. This list provides visibility on all available physical books in AgileLab offices. This list helps you answering the following questions:
- What books are available in my office?
- What books are available in other offices?
- Is there a book that can help me with my learning path?
- Is there a book that can help me with a current task?

## How to buy new physical books
- Follow the book purchasing process on [SpendingCompanyMoney](SpendingCompanyMoney.md) page.
- After buying a new book, [update the list of available physical books](#how-to-update-the-list-of-available-physical-books).

## How to update the list of available physical books

The list of available physical books is maintained collaboratively. Please help us maintaining and update obsolete info.

When you order or share a new physical book in your office,
- access the physical book list in `sharepoint://Big Data/Documents/Library/Libreria Agile.xlsx`,
- in the `Current` tab, please add book title and office info in a new row.


## Digital library
You can find some DRM-free digital books in `sharepoint://Big Data/Documents/Library`.

## How to buy new digital books
- Follow the book purchasing process on [SpendingCompanyMoney](SpendingCompanyMoney.md) page.
- If the book is DRM-free, [contribute the book to the digital library](#how-to-contribute-a-book-to-the-digital-library).

## How to contribute a book to the digital library
After buying a new, DRM-free book, 
- go to the digital library `sharepoint://Big Data/Documents/Library`
- create a new folder named after the new book title,
- add the new book file in this folder.

Help spread the word about this new book! Please post a short message on Teams, BigData/library channel including:
- book title,
- link to the new digital library folder,
- (optional, but recommended) why you choose this book,
- (optional, but recommended) if you read this book, how it helped you in your growth path - or how it didn't,
- (optional, but recommended) if you haven't read this book yet, what interests you the most about this book.





