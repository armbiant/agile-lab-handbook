The onboarding process for the Engineering Department is the starting point for all the new people joining Agile Lab (regardless of status, interns should be onboarded in the same way).

# Checklist for Administration

- [ ] Inform Internal IT about new incoming members (starting date) via mail.
- [ ] Inform Internal IT about devices that will be delivered to the new member (notebook, cell phone, others...)

# Checklist for Internal IT

- [ ] Create an account on Office365 "name.surname@agilelab.it" and send credentials to the private email.
- [ ] Double check ( also on AD ) if MFA has been enabled
- [ ] Include it on group "dev".
- [ ] Include it on "BigData" Sharepoint.
- [ ] Include it on the specific branch (city) group.
- [ ] Check that new member devices are compliant

# Checklist for the Lead Link

- [ ] Create an account on "Elapseit" and send credentials to "agilelab.it" email.
- [ ] Assign an onboarding buddy on the new employee's team.
- [ ] Send an email to the new employee and the buddy linking this page to let them be aware about next steps.
- [ ] Organize smooth onboarding with clear tasks. Define which technologies should be studied in the first two weeks.
- [ ] Share the tech interview outcome with the assigned buddy so to allow her/him to perform a complete tech assessment of the newcomer.
- [ ] Include it on first project Sharepoint.
- [ ] Include it on recurring activities and meetings for the first project.


# Checklist for Buddy

- [ ] Read the buddy section in the [leadership guidelines](https://publicagilefactory.gitlab.io/handbook/Leadership.html)
- [ ] Introduce yourself to the new team member with a video call.
- [ ] Remind about the handbook, suggest most useful pages, clarify it in case of need.
- [ ] Describe self-management practices, provide a high-level overview of the circles and invite the new colleague to attend as a guest a tactical meeting.
  Make sure they know that they are highly encouraged to join a circle and that they know how to proceed if they want to join one.
- [ ] Describe the Data Engineering practice documents, their purpose and how they contribute to elevate the data engineering game. Suggest that the newcomer reads [Pratices page](https://agilefactory.gitlab.io/developers/practices/) and review with them some key lessons
- [ ] Describe the OKRs goal-setting framework with the support of the [OKR page](https://publicagilefactory.gitlab.io/handbook/OKR.html). Suggest that the newcomer watch the OKR workshop on LMS and the resources provided in the OKR Teams channel (under the Resource tab)
- [ ] Talk about Agile Lab values and how to collaborate with peers.
- [ ] Explain compliance procedures and GDPR policies.
- [ ] Talk about the engineering ladder.
- [ ] Introduce her/him to Teams (note: new team member will be automatically inserted on major channels).
- [ ] Check in regularly about her/his first tasks status and clarify doubts.
- [ ] Ask the new team member to fill-up the Skill Matrix.
- [ ] Leveraging the skill matrix and the tech interview outcome received from the Manager, perform a tech assessment of the new team member.
- [ ] Support her/him on training tasks for the first two weeks. The training program must be developed considering the subjects addressed by the Manager and the skill-matrix-based assessment in order to also fill up the knowledge gaps on "first class citizens" skills like version control tools and principles, issue tracking, SW development best practices and patterns, unit testing, etc ...
- [ ] Introduce the new team-member to 5 people and keep track of these people filling in the `sharepoint://Training/Documents/Buddy/onboarding_introduction_calls.xlsx` file. The new team-member's name should be added to the list in order to make him/her available for future calls. Please, don't schedule welcome calls with other Buddies: they would love to help, but they are already engaged in onboarding other people, so let's try to distribute the pleasure across the entire organization
- [ ] Talk about the [software development channel](https://web.microsoftstream.com/channel/e9dec84c-87c6-430a-b109-1964847d6bfe) on streams, since there some useful videos for training and deep dives
- [ ] Talk about the role of the {% role %}The elevator/Data Practice Mentor{% endrole %} before to say goodbye.
- [ ] Talk about the [awesome resources repo](https://gitlab.com/AgileFactory/developers/awesome-resources)
- [ ] Talk about [stackoverflow for teams](https://stackoverflow.com/c/agilelab/questions), (our internal StackOverflow), the benefit of it, like sharing issues (not only technical), and solutions so that all the company can benefit from it. Invite (on StackOverflow page, users -> invite users) every new ENG 1 team member or new members that seem very engaged by this initiative.
- [ ] Share an onboarding outcome with the Manager and/or the Project Lead the new member will be assigned to, focused on the completed training program so to bring light over strong and weak points the Project Lead might need to take into account (e.g. if there was no time to train the new team member on a certain required technology or tool, let's say git for example, the Project Lead must know about it before assigning him/her the first actual development task)
- [ ] Share the list of workplace safety managers (RLS, first aid, fire prevention) to the new members. The list is available in `sharepoint://BigData/Documents/People Operations/Responsabili sicurezza`

# Checklist for new team-member

- [ ] Login into Office365 and check provided credentials are working.
- [ ] Login on Elapseit and check provided credentials are working.
- [ ] Login on Holaspirit and check provided credentials are working.
- [ ] Login on Teams.
- [ ] Check you can access to "BigData" Sharepoint.
- [ ] Follow [this guide](DeviceEnrollment.md) to enroll your devices.
- [ ] Contact Internal IT to verify that installation is completed or if you need help.
- [ ] Introduce yourself on the "intros" Teams channel so to let your (especially remote) colleagues know a little bit more about yourself, what do you like, area of expertise, location, etc. Although this is not mandatory, it's highly recommended for the sake of the value of being a company focused on the individuals as persons, not as just anonymous workers. Make sure to write your message in English so that all your colleagues can learn about yourself.
- [ ] Configure your Office365 profile picture. Use a photo of yourself to make you recognizable by other agile lab employees.
- [ ] As soon as you receive your laptop (and screen) ask your Facility Manager to register them in the People Operations census file.
- [ ] Schedule a call (30 minute) with the {% role %}Training/Certification Coach{% endrole %} in order to know the certification plan and the related opportunities
- [ ] Schedule 5 introductory calls (30 minute each) with the 5 people provided by the buddy.
- [ ] Exploit the alignment sessions with your buddy to ask any clarifications (technical, about the company, the internal processes, etc) you might need.  
- [ ] Add your signature in the Outlook email.
- [ ] Send a photo of yourself and your role to {% role %}Digital/Social media specialist{% endrole %} for the welcome post on LinkedIn and Instagram.
- [ ] Fill in the Excel `sharepoint://BigData/Documents/Sito Internet/Sito_paginaTeam.xlxs` to give authorisations and information to public your profile into AgileLab site. It's not mandatory to have the profile in Agile website: in this case you should fill the authorisations fields with `no`. In the future, you can change some information setting the field `status` with `ToBeModified`
- [ ] Fill in the data in the directory `sharepoint://BigData/Documents/Sito Internet/Sito_paginaTeam` with you name, surname, role, phrase/quotation that represents you, link of you LinkedIn profile.
- [ ] Add your photo (named `<name>_<surname>`) in the directory `sharepoint://BigData/Documents/Sito Internet/FotoPaginaTeam/`. This step is optional but recommended. 
- [ ] At the end of the first two weeks, fill out the Onboarding Quiz: for the Engine people there is this form `sharepoint://BigData/Documents/Onboarding/Engine onboarding Quiz.url`, for others (marketing, sales, HR, etc.) there is `sharepoint://BigData/Documents/Onboarding/GCC onboarding Quiz.url`

  Outlook *app* users: access instructions and template in `sharepoint://BigData/Documents/Doc Templates/Template firme mail.docx`.

  Outlook *web* users: 
  - open Firefox 
  - access the template in `sharepoint://BigData/Documents/Doc Templates/FIRMA MAIL/firma email agile lab.htm`
  - select all (CTRL+A) and copy (CTRL+C)
  - open Outlook
  - in Settings, search Email Signature
  - in the signature text field, remove all contents
  - paste (CTRL+V) the templated signature
  - fill in your details (full name, title, email, office address).

# First two weeks

In Agile Lab the first two weeks are dedicated to train on different technologies based on Manager indications and Buddy's assessment after the new team member has filled up the Skill Matrix. The Buddy will be supportive along this period.

In "BigData" Sharepoint there are the official Agile Skill courses that must not be shared with external world. This material can be then integrated with other resources in case of need.





