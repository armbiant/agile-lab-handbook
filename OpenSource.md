# Contributing to Open Source

AgileLab is a proud user of open-source technologies, we receive useful tools made by the community and we want to give back what we received by contributing to open-source developement.

Letting you to contribute on open source projects will act as a training program. Being able to contribute to open source projects will improve your skills all around and the company will benefit from this. It is a win-win approach and a good way to raise the bar of our technical and organizational skills.  


To be able to contribute effectively during company time we adopt this set of rules:

*   The COO will allocate a total number of days that can be devoted to opensource development for a given timeframe
*   Developers who want to contribute to opensource development must perform a pitch to other developers about the scope of the contribution, this pitch will be delivered in the form of a paragraph describing the contribution and the estimation in hours to be spent on the contribution, as a Gitlab issue logged on the project AgileFactory/developers/oss-contributions
*   Other developers will vote the different contribution proposals to acknowledge that the time subtracted from the common pool will be well spent and won't be subtracted from other useful or potentially more successful contribution
*   If at least X developers vote in favor of the contribution then the proposer of the contribution is entitled to start his/her journey in delivering the contribution and make sure that it is accepted
*   When a contribution is finally accepted by the opensource community the issue with the initial pitch is updated with the relevant info (pull request link, outcomes, lessons learned) and the days spent are subtracted from the common pool
*   We should be able to record info and have a retrospective even on a failed attempt to get the contribution accepted, so the issue will be updated even after a bad outcome of the contribution, days from the common pool will be spent anyway
*   We will maintain a leaderboard of top open-source contributors inside the company to encourage contribution within this framework of rules
*   Contributions will be sponsored by the company with posts on social media and maybe a public leaderboard on AgileLab's website


The time spent on this activity should be logged on elapseit as "Formazione Interna" and it must not affect project deadlines and customer satisfaction. 
Keeping this in mind, it is your own responsibility whether or not to inform the customer when you spend time on this, but in any case Agile Lab will **never** bill to a customer the time spent on these activities.

Everyone is free to integrate the time provided by the company using their own free time.


Contributions are valid in case you work on one of the following projects (whitelist), otherwise you need an approval from the CTO:

*   Apache Spark
*   Apache NiFi
*   Apache HBase
*   Apache Solr
*   Apache Kafka
*   Apache Pulsar
*   Apache Ignite
*   Apache Cassandra
*   Apache Ozone
*	Apache Druid
*	Apache BookKeeper
*	Apache Calcite
*	Apache Flink
*	Apache Kudu
*	Apache PredictionIO
*	Apache Zeppelin
*	Apache Drill
*   Apache Hudi
*	Apache Arrow
*	Apache CarbonData
*	MLFlow
*	DeltaLake
*   Kubernetes
*   Kubeflow
*   Seldon
*   OpenVino
*   Polynote
*   Iceberg

