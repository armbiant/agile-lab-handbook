# Buddy

The buddy is the role in charge of onboarding new team members. The onboarding period last 2 weeks, and the main goals of the buddy in these amount of time are:
- organize and support the new team member study about the tech stack
- explain our internal processes and transmit the cultural values that make Agile Lab work and evolve step after step

### Internal Sharing
The list of the topics that should be covered by buddies during each onboarding can be found in the [onboarding section](https://handbook.agilelab.it/Onboarding.html). The list highlights what to explain, but not how to do that, on which aspects put more focus, how to handle new team members with different seniority, and so on.

It's ok to not having a common way of explaining things, because it allows buddies to use their creativity, experience and empathy to design the best experience candidate after candidate. In Agile Lab, we leave total freedom to people about their way of working, but, at the same time, we strongly believe the socializing ideas and using collective intelligence to solve complex problems elevates our practices to the next level.

That's why we introduced the "buddy internal sharing", a recurrent meeting where buddies can discuss old onboarding experiencse and brand new ideas for the future ones.

These are example questions that can perfectly fit this kind of meetings:
- "How do you explain the handbook? Do you just say that is a process collector or do you bring some example to the table?"
- "How do you onboard an engineering manager?"
- "How do you onboard a new team member already proficient with our tech stack?"
- "How do we introduce our culture in smooth way to people that comes from a rigid corporate culture?"
- "Did you ever onboard two new team members at the same time? Do you think it was productive?"
- "Are you able to create high engagement at the end of the onboarding weeks?"
- "Are two weeks enough to onboard new team members?"

Of course, this is not the list we use as the agenda of the meetings, it just brings you an idea of the kind of topics discussed in these sharing sessions.

Buddies should periodically discuss these kind of topics to iterate, getting feedback, and crafting the perfect onboarding experience for new team members.