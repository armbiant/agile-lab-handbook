The change management process is applied when we need to create an economic offer for a customer, and this could happen in two circumstances:
* a new turnkey project is starting
* a CR ( change request ) has been requested

The flow is the following one:

### 1) Request:
The request is the stage where we receive a requirement. If we are starting a new project, the Sales team will raise the requirement. Otherwise, in the case of CR, it is often the customer himself who qualifies it. 
The requirement, especially in the case of a new project, potentially is a high level. In any case, we need to be able to conclude the process. The Sales team or the customer should send it to the COO that will involve the right people to complete the process.


### 2) Evaluation & Feasibility:
The engineering team ({% role %}Engine/COO{% endrole %} + involved people) must evaluate the request to understand if our company is a good technical fit for that.
If the requirement is too high, a deep dive with the customer is needed to understand the request better. In this deep dive, the engineering team must be present, don't use the sales team as a bridge unless there is a pre-sales architect.
Also, in this phase, it must be clear the definition of done for the customer in terms of expected deliverables. To conclude this phase, the COO must provide a **go no go** towards Sales. After this moment, the engineering team will not have other chances to change ideas about the project feasibility.


### 3) Estimate:
If the project is feasible, we have to estimate the effort. The engineering will elaborate on the estimate, and the COO will review it. Pay attention to include all the possible activities in the estimate because there will not be another window to adjust the offer. 

Here a list of activities that you should consider:
* Development
* Testing ( unit, integration, performance )
* Documentation
* Project Management
* Infrastructure setup
* Security
* DevOps
* Contingency
* Requirements analysis
* HLD & LLD

This list could change based on the list of deliverables requested by the customer.

General tips to make a reasonable estimate:
* Look at past similar projects and real delivery speed
* Try to weigh the effort on an average level team ( at this time of the process, the team that will implement the project is still not known ), not on your skills.
* If you are in doubt, add contingency.

The estimate must be done in an Excel file, splitting as much as possible the activities and documenting the numbers' reason. Remember that an estimate could be reviewed several years later when nobody will remember the context, and it should be comprehensible.

The COO will try to challenge the estimate from different standpoints before to freeze it and to propose it to the Sales team. 
When an estimate is frozen, the COO should archive it in the Engine Sharepoint under the folder "estimates" -> "$customer" -> "$project," choosing an excellent filename to make it recognizable.

The COO will also set the time frame needed to put together the new team if we get an acceptance from the customer.


### 4) Offer:
This phase is in charge of the Sales team. They will evaluate the estimate, and they will try to convert it into an economic offer for the customer.
If the Sales team thinks that with the current landscape is not possible to create a proposal that will be appreciated by the customer, they will try to cut activities or re-arrange them to find a better fit for the customer. In this case, the Sales and Engineering teams should iterate through the points 1-2-3.


### 5) Acceptance:
The customer has accepted the proposal. The Sales team must communicate it immediately to the COO to start the next phase.

### 6) Planning:
The COO will update the capacity plan and will communicate with the team about the new activities.

### 7) Execution:
Delivery, Delivery, Delivery








