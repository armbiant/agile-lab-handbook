## Compensation principles

( not applied to consultants and contractors )

The salary review process happens every six months because people continuously increase their skills and capabilities, and their compensation must follow accordingly.
Compensations in Agile Lab are based on the following principles:
- Pay top of our reference market
- No speculation on people's cost of life ( family situation, residence region, etc.) or other factors ( gender, age, etc.)
- Internal fairness, everyone should fit into the ladder


***Reference Market***

Our reference market is defined by where our customers are based because our revenues strictly depend on this factor. If we mainly work with Italian customers, our "reference market" is Italy for salaries. When we shift our customer base towards other countries, we will adjust the compensations accordingly.
When possible, during the recruitment process ( typically after we made our proposal ), we collect information about the previous compensation of people we are interviewing to gain a 360 view of the market based on roles and seniority. 


***Top of the market***

At the same time, we measure the effectiveness of our hiring proposals to understand if we are paying top of the market.
If we can satisfy candidates in at least 80% of the cases with our proposals, we are on the right path.
All our proposals should be compliant with ladder ranges, and as soon we detect that our offers are not compelling anymore, we are falling behind the market. 
In that case, the ladder and its ranges will be reviewed, and this will positively affect all the people currently employed in Agile.


***Fairness***

Within each ladder level, we evaluate if people's compensations are balanced and if they are a good mirror of their performances, impacts and contributions.
Because new people are coming into the picture each semester, it is essential to re-evaluate the fairness across all the people with the same frequency.


***Salary and Ladder relationship***
Within each ladder level, we have a salary band. We have a band for each level because it is impossible to be 100% fair and objective every moment we observe the ladder. The positioning of a person in the salary band is not a direct proxy for its positioning within the ladder level. Each person has their history, past salary, seniority, and performances that must be considered.
We could have a person that is top of the Eng3 salary band but is far from being promoted to Eng4 and vice-versa. What we value is the trajectory, so if a person is making significant improvements, it will have the proper reward in terms of salary increase. Otherwise, it will not.



## How to

Salary increases are defined by the Business Unit lead links or in fall back (if the person is not part of a BU) by the Engine lead link. Anyway in case there are not at least 2 levels between the BU lead link and the person that is under evaluation, the review will escalate until we are able to match the difference of 2 level. For example, the Eng 4 salary can't be reviewed by an Engineering Lead, but it should be reviewed by an Engineering Director or the VP of Engineering.

The specific person is not involved in the salary review process because we adopt a pro active approach, but in case he/she is not satisfied or you want to know more, he/she can always reach the person in charge of the review according with previous mentioned rules. Keep in mind that this is not an individual process, and salary review aims also to bring equality across the company, so is not always possible to provide all the information about such process because it is highly sensitive one.

Once the ladder ranges have been reviewed, how to decide if a salary increase is needed or not?


***Not the case***
- They are not improving
- They are not engaged
- They are toxic or negative
- With a raise they would go above the upper limit of the ladder level range


***should be considered, but not due***
- They are performing good, showing some improvement, but not consistent
- Not getting a salary improvement since one year or more
- Acquired some specific skills valuable for the company


***Is needed for sure, no doubt, even if they got a raise six months ago***
- They are falling behind the ladder level they belong to
- They are *clearly outperforming* another person within the same ladder level but with a higher salary
- They demonstrated a tangible performance boost and authentic engagement since the last raise


These three blocks should be evaluated in this order. In addition to this every year the Engine Lead Link is defining a cap for salary raises, for example 3% of existing salaries, and all the business units should be stick with such cap.




## Constraints

At the beginning of the year, we define a budget for salary raises, typically a percentage quota ( ex. 4% ) that should be managed for the whole year.
