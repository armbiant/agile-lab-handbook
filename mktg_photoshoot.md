# Introduction
Occasionally, Agile Lab offices may arrange photo shoots on their own for marketing reasons. 

There are two main reasons to hold this type of event: 

- Getting photos for some social media post or campaign (e.g. Linkedin or Instagram). 

- Getting some photos for Agile Lab websites. These photos can be used for the Team page or other pages on the websites. 

# Process/Checklist

One person is designated to organize the event, usually the one who will take the photos. 

To make good use of the time devoted to this activity, the organizer must: 

- **Be clear about the purpose of the photo shoot**. This means deciding what the photos will be used for (social media or website for example). 

- **Be clear on what type of photos you intend to take**. Photos for the team page of the website are very different from those for other pages (portraits vs generic photos of spaces and merchandise). 

- **Make sure you have what you need to be able to take the pictures you intend to get**. This includes the device choice (smartphone vs a more professional camera), the locations choice (office vs outdoor space) and other useful tools and items (merchandise, computers, people outfit, etc). 

- **Choose the people who will participate in the photo shoot and make sure they know the purpose of the event**. Estimate how long it will take to achieve the desired result and make sure the participants are available for the time needed on the event date. If it is not necessary for all participants to be present at the same time, arrange to work in shifts so that everyone's normal activities are affected as little as possible.  

- **Plan the photo shoot in advance**. The person running the event is in charge of setting up the location where the photos will be taken, placing props, preparing backgrounds, arranging lighting and thinking about angles and focal lengths of shots (wide angle vs macro).   

- **Guide people through the photo shoot**. The person directing the event has the clearest idea of the desired result and how to achieve it, and is responsible for guiding people into the various positions and poses and arranging the scene.  

# After the event

Following the event, the organizer will take care of any post-processing of the photos and of delivering them to the people responsible for social media and updating the website. 
