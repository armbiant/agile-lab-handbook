Agile Lab spans across six physical locations:
* [Bari](Bari.md)
* [Bologna](Bologna.md)
* Catania
* [Milano](Milano.md)
* Padova
* [Torino](Torino.md)

Every office come with a different set of amenities.
