#Language Learning
Agile Lab has adopted English as the official language. 
Some people may not feel too confident speaking English, or someone may want to work on her/his accent (like the Italian one). For this reason we introduced the possibility to improve our language skills using a platform that offers 1-to-1 sessions with native speakers.

##Budget
Lessons cost money and time. 
Fortunately, it is possible to use the (money and time) budget that Agile Lab allocates for training as explained in [benefits](Benefits.md).
Alternatively, it's possible to use part of the past Company Prizes to allocate the budget to the platform.

##The platform
Each employee can decide to dedicate a part of his/her budget to the platform. 
It is suggested to plan at least 1 hour per week for 3 months.  
More details about the platform and how to add budget to the platform are described in a dedicated wiki of the channel English Learning of BigData in Teams.

##Languages
At the moment it is supported only for English and Italian Learning. 
English, as said before, is the official language of Agile Lab.
For not Italian people can be a good opportunity to learn the Italian language on the platform and after exercise it with other Italian employees during the job.
