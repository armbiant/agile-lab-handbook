## Health Insurance

AgileLab provides an additional health insurance issued by Generali for all the employees. The health policy covers a lot of medical services, such as specialist examinations, dental care, surgical procedures and so on. 


In order to benefit from this health policy, you need to: 
1. Fill out the insurance module received by Generali. Every three months a subscription module is sent to all new employees.
2. If you haven't already done it, create an account on [MyGenerali](https://areaclienti.generali.it/AreaClienti/area-clienti.html#/home). All the employees should be already associated with the company name and its relative insurance plan (called "Previgen"), so the registration should take just a few minutes;
3. On the Agile Lab Health policy page, select the company you refer to, and in the following page you will find four buttons that redirect you to the detailed page for each section. 

    - _PRENOTARE PRESTAZIONI MEDICHE_: in this page you will find everything you need to know to book a medical service. In this way, Generali takes care of directly paying the medical service (apart from a small amount, called deductible)
    - _RICHIEDERE RIMBORSI_: in this page you will understand how to ask for a refund of a medical service you already have benefited.
    - _AVERE INFORMAZIONI_: important page in which you will find the consultable insurance contract with all the information related to the policy.
    - _RICHIEDERE ASSISTENZA_: contains a form useful for asking for support.



The starting point for taking advantage of this benefit is the Generali's health policy page dedicated to our company: [Piano Sanitario Integrativo Aziendale](https://sites.google.com/realtointermedia.it/gruppoagilelab/home). Here, you will find all the detailed information you need. The purpose of this Handbook page is to summarize that information and act as a guideline for the process.
Before pointing out all the necessary steps in order to access this policy, it is important to understand a few things:
- Generali gives you two possibilities for covering the medical services costs:
    1) _Prenotazione diretta_: it means that Generali takes care of directly paying the medical structure you are going to contact for a medical service. This is the preferred way because it is generally the way your deductible ("franchigia" in Italian - the cost that will be up to you anyway) is the least amount and it's generally a fixed small amount. 
    2) _Richiesta rimborso_: it means that you pay in advance for the medical service you need. Then, you ask Generali for a refund. Generally, it's never a full refund because as per the Prenotazione diretta, there is a small deductible left up to you. In this case, if you request the medical service outside the Generali network (which means that either the structure or the doctor, or both, are not Generali affiliated), the franchigia is generally a percentage of the total cost (for example, the 30% of the medical service cost).
- For all the medical services you are going to require, you need to have your doctor's request ("_prescrizione_"). Exception made only for the dental care. 
- It is important to take a look at the full contract of our health policy that you can find [here](https://drive.google.com/file/d/1uPoqLMqa20W2pwq0qvcKKImO45B10naS/view). In particular, it contains the table of the maximum coverage you might ask for in a year together with the deductible amount for each specific health service.

### Direct booking ("Prenotazione diretta") ###
In order to ask for a direct booking of a certain health service, you first need to choose an affiliated health center. Besides that, you will also need to look for an affiliated doctor. This [page](https://www.generali.it/strutture-convenzionate/strutture-mediche) will help you find both. 

Once you have chosen both the structure and the doctor, you can forward your request in two ways:

1) Via phone (all the details here: [Come Prenotare](https://sites.google.com/realtointermedia.it/gruppoagilelab/home/prenotare?authuser=0)) 
2) Via web form: you just need to log into [MyGenerali](https://areaclienti.generali.it/AreaClienti/area-clienti.html#/home), then click on "Persona e salute", then "Richiedi rimborso o prestazione" and finally follow the guided procedure. 

### Refund request ("Richiesta rimborso") ###
If you have already done a medical examination or any other kind of medical service convered by the policy, you might want to ask for a refund. The procedure shoul be opened on MyGenerali. Once you are logged in, go to "Persona e Salute" and at the bottom of the page you will see "Richiedi rimborso o prestazione". You will click there and this time you will choose "Richiedi rimborso" in the following page. Then, just follow the guided procedure. Don't forget to upload all the necessary documentation otherwise your request might be rejected. 


