In Agile Lab, we search for smart and reliable people. We don't care about skills and experiences. Our aim is to hire the best candidates around the world, leveraging our remote-first attitude.
We are best in class (or at least we would like to be) in our domain, so people will learn everything is needed inside the company.

# Principles

Our hiring process reflects our company values.

- Collaboration
    *   Lean on our company for their global talent networks
    *   Develop a rich and robust referral program
    *   Foster cross-company understanding of roles and open positions
    *   Distribute ownership of team building and hiring across full company
    *   Encourage internal applications, support career transitioning
- Results
    *   Identify and source high potential candidates from varied industries
    *   Build long-term hiring forecasts and talent maps
    *   Make talent a long term priority with a deep true vision and strategy
    *   Hire the right person for the right role at the right time
    *   Transform talent from a cost center to an organizational value add
    *   Measure employee engagement and candidate experience
- Efficiency
    *   Respect candidate and employee time in hiring
    *   Create a repeatable, standard process that is well documented
    *   Support training and knowledge transfer
    *   Source for candidates in Low-Cost areas
-   Transparency
    *   Document the hiring process and keep it updated
    *   Make sure the candidate knows in advance the process and the timing
    *   Give every candidate an answer.
    *   Share insights and hiring achievements often and with everyone
    *   Provide honest and kind feedback when asked (try to separate the interview from the decision, and from the feedback)
    *   Share hiring plans internally and announce opening positions before publishing

# Roles

## Recruiter

The Recruiter steps are:
* Check the denylist in `sharepoint://BigData/Documents/HR/denylist recruiting`
* If the check is positive, contact new candidates and update the file `sharepoint://HR/Documents/Recruiting/Lista Contatti`
* Upload the candidate CV into an open position and fill all relevant fields, including a Tag to define who provided the contact.
* Contact the candidate for a Screening Call/Introductory phone call, create new feedback on the candidate profile and move it into the "contacted" phase in Talent Center
* Email the candidate with the link of the Screening Test and then move it into the "written test" phase in Talent Center

## Interviewer

#### Steps

The Interviewer steps are:
* Check the results of the Screening Test, and add feedback on the candidate profile
* Email the candidate with the result of the test and, if it has been successful, try to schedule the Technical Interview
* The interviewer should assign himself as a candidate's owner into the hiring tool.
* Once the Technical Interview has been scheduled, move the candidate into the "Technical Round" in Talent Center.
* Run the interview:
  * If the candidate fails the test, "refuse" him/her on the tool specifying the reason "Failed in challenge round". The candidate will exit from the funnel.
  * If the candidate manages to succeed:
    * Define your general recommendation: strongly hire, hire, neutral, don't hire, strongly don't hire
    * Define the engineering level
* In any case, add new feedback into the candidate profile in Talent Center

#### Interviewer Guideline

1. Maintain candidate confidentiality
2. Those on the interview team should prioritize the interview in their schedules
3. During the Screening Call, remind the candidate to look at this handbook
4. Compensation is discussed at start and end but not in between
5. The entire process should be over in two weeks

## Hiring Manager

* If the candidate is Eng. 3 level or above, conduct the Cultural Fit session.
  * If successful:
    * Conduct the Offer phase and add feedback to the candidate profile in Talent Center
    * Make an economic proposal and move the candidate into the "Offered" phase
    * When you get the feedback from the candidate, move it into the "Offer accepted" or "Offer declined" phase. If declined, select the specific reason.
  * If not successful:
    * Move the candidate into the "Not a cultural fit", and add feedback to the candidate profile in Talent Center

# The Process

Below we outline the recruitment process, describing all its phases.

The recruitment process can start in two different ways:
*  The candidate applies to an open position 
*  We get in direct contact with a potential candidate.

## Voluntary application

If a candidate voluntarily applies to an open position, we just do the **Background check**:

* We verify if the background of the candidate is compatible with the open position
  * If the background check is positive, the Recruiter arranges a brief **Introductory phone call** and then sends an email to the candidate with a link to the **Screening Test**. 
  From here on, the responsibility shifts from the Recruiter to the Interviewer, who will be responsible for making the Tech Interview.
  * In case it is not, the Recruiter decides if the candidate is suitable for another open position

## Proactive application

If we are actively contacting a candidate, it is strongly recommended starting with a **Screening Call**.

## Referral

Agile Lab [benefits](Benefits.md) also include the ability to earn a bonus via Referral.

In order to be valid, the Referral must be "proactive": the Referrer (i.e. the employee in Agile Lab) must actively recommend a new person (from now on named _candidate_).

There are two types of Referral fees:
- Type A: Employees can suggest a friend, an ex-colleague, a classmate at University, so a trusted person who will bring added value to the company, with at least 2 years of experience. If that person is hired successfully, the referrer shall be prized with € 1.500 gross value that will be paid when the trial period of the neo-hired person will be over.
- Type B: Employees who suggest people met in a corporate or private setting, or online (webinars, social media, chats such as Telegram, and so on), and tell them about the positive experience in Agile Lab, convincing them to send their CV. If they have at least 2 years experience, and any of them are successfully hired, the referrer will be rewarded with €200 Amazon gift that will be paid when the trial period of the neo-hired person will be over. Only 2 type B referral are available per year per employee.

Please note:

If the candidate, type A or B, has been already previously contacted by Recruiters, the Referral will be not valid.
Referral of newly graduated, or without experience, candidates will always be greatly appreciated, even if it will not result in any benefit.
We cannot hire people from all companies, please take a look at the recruiting denylist before suggesting a candidate: yoiu can find the list in the `sharepoint:BigData/HR/denylist recruiting.xlsx`file.

What to do:

Employees can send the CVs to Recruiters and tell them what is their relationship with the candidate and the consequent Type of Referral.
If the candidate is eventually hired, the Recruiter will send an email to the Administrative Deparment, to indicate to whom the referral is due: it will be paid, as said as above, after the end of a successful trial period.

## Introductory phone call

The introductory phone call is helpful to evaluate a job candidate’s qualifications.

During this step:

* Ask questions about his/her previous and current work experiences
* Ask questions to determine if the candidate meets the requirements outlined in the job description
* Ask questions to establish if the opening matches what the candidate is looking for
* Provide a brief description of the company
* Inform the candidate, if he/she is suitable to continue the selection process, that the screening test will be sent to him/her by e-mail after the phone call

The transcript of the Introductory call should be reported in our Talent Center as new feedback by selecting the "contacted" step.

## Screening Test

*   An online test to assess general and specific knowledge of the candidate
*   Duration can last from 30 to 70 minutes, depending on the test
*   It expires after seven or fourteen days, depending on the test as well

In any case, you will receive the result of your test. If you passed it, we will propose you available slots to run the **Technical Interview**.

## Screening Call

#### Steps

* One of our recruiters will conduct a screening call using Skype
* This will be scheduled respecting candidate timing.
* The purpose of this step is to present the company to the candidate and understand if resume information is consistent.
* It must be also ensured that there is still mutual interest before proceeding with the next step.
* Interview process is a considerable investment of time, so if you are not interested, please drop at this stage. Don't interview for curiosity or other purposes.
* The recruiter will wait for 5 minutes for the candidate to show up at the meeting. If the candidate does not show up to the interview, or he does not reach out in advance to reschedule, the candidate will be classified as a "no show" and be disqualified.
* One can terminate the discussion early at any point during the interview if either party determines that it isn't a fit. Be as transparent and honest as possible and provide feedback.

#### Conducting a Screening Call

Calls can last anywhere between 30 and 60 minutes, depending on the conversation.

Example questions include:

1.  Why are you looking for a new position?
2.  Why did you apply with Agile Lab?
3.  What are you looking for in your next position?
4.  Why did you join and leave your last three positions?
5.  What is your experience with X? (for each of the skills listed in the position description)
6.  What did you learn from your last project? (verify alignment with resume)
6.  What is the notice period you would need if you were hired?
7.  What are your compensation expectations?

Then, remind the candidate about our handbook and tell about life in Agile Lab: values, results, and projects (without details about the customer and other sensitive data).

Conclude the call, checking for mutual interest in proceeding with further steps. Usually, we provide final feedback about if you have been selected for the next step or not within two working days. In case of positive feedback, we also provide a possible schedule for the next round, the Screening test.

The call's transcription should be reported in our Talent Center as new feedback selecting the "contacted" phase.

## Technical Interview

#### Steps

* This is a 1.5-hour technical interview on Skype
* The interview has a theory part that will follow the format of Bar-Raiser, and it will not require knowledge about specific technologies but only general concepts.
* The Recruiter will wait for 5 minutes for the candidate to show up at the meeting. If the candidate does not show up to the interview, or he does not reach out in advance to reschedule, the candidate will be classified as a "no show" and be disqualified.
* One can terminate the discussion early at any point during the interview if either party determines that it isn't a fit. Be as transparent and honest as possible and provide feedback.
* After the interview, immediate feedback can be provided; otherwise, it will come within two working days.
* If the candidate had not a proper presentation of the company, this will be covered here.

After this stage, the candidate will be collocated on our Engineering Ladder. If you are Eng.1 or Eng.2 and the Technical Interview had a positive result, we jump into the **Offer phase**.
If you are Eng. 3 or above, there is a further step that is the **Cultural Fit**.

In any case, from now on, the responsibility shifts from the Interviewer to the Hiring Manager.

#### Conducting a Technical Interview

Calls can last anywhere between 90 and 120 minutes, depending on the conversation.

* Follow the interview template available on HR Kit in GitLab
* Add a new Feedback on Talent Center, selecting "Technical Round" phase and writing all the questions and answers for each of them.
* Try to spot all the candidate's weakness areas: it will be useful to define the training path during the onboarding phase.
* Try to assess which level on Eng. ladder the candidate is fitting

## Cultural Fit

This is a 30-minute behavioral interview done by the Hiring Manager on Skype.

The Hiring Manager should lead a discussion focused on:
* values
* principles
* organization
* leadership

that characterize the culture of the company.

## Offer phase

* The Hiring Manager arranges a call on Skype with the candidate, if needed 
* The candidate will receive an offer (verbally) that will be followed by an official contract the day after.
* The offer will be valid for five working days since the reception of the official contract.

# Process of opening new selections

If a Lead Link needs new people for its circle, and it has the budget to proceed, he/she can contact the Recruiter role to describe the needs. The recruiter will create a project on Holaspirit and carry out the selection process.

# Talent Center

In Agile, we manage the recruiting pipeline through our Talent Center (FreshTeam).

# How to apply

Do you absolutely want to get in touch with us? That's great news, we are waiting for you! The best way to apply is through our Talent Center: https://agilelab.freshteam.com/jobs

You will be contacted as soon as possible. We hope you will join our team!

# Retry Policy

A candidate can re-enter the recruiting process funnel after a period of 1 year.

During this period, the candidate should independently work through the feedback provided during the process and attempt to address identified weaknesses.

The recruiting process, in case of re-apply, will be started from scratch.





