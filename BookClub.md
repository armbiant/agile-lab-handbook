# Book Club

The Book Club is an initiative of the Training Circle. It is run by the {% role %}Training/Book Club curator{% endrole %}.

## FAQ

### What is the Book Club?
The Book Club is a community where we read to expand our knowledge and inspire knowledge sharing.

### What is the goal of the Book Club?
The goal of the Book Club is to promote:
- learning something new (continuous learning)
- learning something useful (real-world applications)
- learning from each other (discussion).

### What do we do at the Book Club?
We read iteratively. A reading iteration is usually [timeboxed](https://www.agilealliance.org/glossary/timebox/) at two weeks.

Every reading iteration,
- We read something new (eg book chapter, blog post) in async.
- We collect discussion points in async, based on a discussion template.
- We meet (30min) to discuss what we read. [How does the discussion work?](#how-does-the-discussion-work)
- We can continue the discussion in async on a Book Club Teams group.
- We distill key concepts and lessons learned from the reading/discussion in form of an [LMS Course](Courses.md). [How do Book Club courses work](#how-do-book-club-courses-work)

### How does the discussion work?
We kick off the discussion with a short (30min) sync session, then we continue the discussion in async on Teams.

In the short (30min) sync session, we review the discussion points that were collected in async. This means we go through the submitted points, and eg questions are answered, lessons learned/tips are presented.

To maximize info exchange in the discussion, the discussion points are based on a discussion template.

In the async discussion on Teams, any reader can contribute with:
- questions about key ideas from the book,
- questions about related topics,
- contents (e.g. videos, blog posts, quotes, or chapters from different books) related to key ideas from the book (e.g. explanations, applications, deep dives),
- lessons learned from applying key ideas from the book to a real-world data project.

### How do Book Club courses work?
We create content for [LMS Courses](Courses.md) with distilled learnings from the reading/discussion in order to: 

1) have an extra opportunity to process and review what we learned collaboratively at every reading iteration - think [protege effect](https://effectiviology.com/protege-effect-learn-by-teaching/) with gamification,
2) enable anytime onboarding of new readers, 
3) enable anytime catch-up for paused readers - life happens and that's ok!
4) give back knowledge distilled from books to the circle:Engine Circle.

### How do we choose our next book?
When a book is completed, we choose a new book.
1. The Book Club Curator collects strategic topics for continuous learning according to the tech strategy.
2. The Book Club Curator selects at least a book for each strategic topic.
3. The Book Club Curator shares the selected books in a poll to all participants of the Engine Circle.
4. All interested readers vote the book(s) they are interested to read in the Book Club, based on current personal interest/goals.
5. The Book Club Curator communicates the chosen book - meaning the top voted book - to all participants of the Engine Circle. 
6. The Book Club Curator adds any new interested reader to the Book Club Teams group. 
7. The Book Club Curator communicates next steps on the Book Club Teams group, including book purchasing details, reading scope for the first session.

### How can I join the Book Club?
You can join the Book Club and start reading with us at any time.
- Please check on Holaspirit who currently energizes the role of {% role %}Training/Book Club curator{% endrole %}.
- Write a message to the {% role %}Training/Book Club curator{% endrole %} and ask to be added to the Book Club.

#### How can I buy the current book?
When joining the Book Club, you can purchase the current book and get reimbursement.

- Please check on Holaspirit who currently energizes the role of {% role %}Training/Book Club curator{% endrole %}.
- Write a message to the {% role %}Training/Book Club curator{% endrole %} and ask what is the current book.
- You can buy the digital or the paperback book and get full reimbursement.
	On Elapseit, record *one* expense: 
    
    1) Use the project: `<current year> Agile - Training - Book Club Curator`

For more info about the general purchasing process, see scenario 1 in [Spending Company Money](SpendingCompanyMoney.md) .

> [!Note] The book is on the house if you decide to be part of our reading journey! If you are interested in the current book without participating to the Book Club, though, you can buy it using your personal Training budget: `<current year> Agile - Training - Learning`.

#### Can I use company time for Book Club activities?
Yes, you can! You can safely use company time (Training time budget) for our sessions.
	
If needed, you can choose to use company time (Training time budget) for reading and preparing for the session.

This means you need to track Book Club activities on ElapseIt as explained in the table below:

| Activity | Project on ElapseIt |
|----------|---------------------|
|Reading outside office hours|Nothing to track|
|Reading during office hours|`<current year> Agile - Training - Learning`|
|Session (during office hours)|`<current year> Agile - Training - Learning`|

For more info about Training time budget, see Training Budget in [Benefits](Benefits.md).
