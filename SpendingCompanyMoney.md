

Spend company money like it is your own money. No, really.

# Office Equipment

The easiest way to buy something for the office or for your workstation is to ask People Operation

Specifically, the employee should contact:
- The Buyer for notebooks, additional screens or smartphones by writing an email to the buyer account

Policies defined in order to replace above mentioned hardware are:
1. replacement may be requested after 3 years of hardware life
2. replacement can be made with hardware of the standard model defined by Internal IT and People Operation
3. replacement can be requested before the expiration of 3 years in case of failure.

- The Facility Manager of your office, writing to his/her email address, for every other daily equipment: Cables, Webcams, Headphones, Keyboard, Mouse/Trackpad, Laptop Stand.
The Buyer and Facility Manager have a company credit card and are responsible for it. They will also be responsible for filing invoices. 

Office equipment must be used for business only.


**Software**

We use Microsoft Office 365, typically the online office suite is enough, but if you need the desktop suite to do your job properly, just ask. 
If you need an additional software that requires a license do the following steps:
*   ask your colleagues if this software can solve also their needs and collect feedbacks
*   send an email to buyer@agilelab.it explaining why it is important and how many people need it.  



**Business cards** 

Business cards must be requested by writing an email to the Buyer



**Work-related books** 

In Agile lab we have office libraries.

If you want to propose a new physical book for a specific office,
- contact the {% role %}People Ops and Care/Facility{% endrole %} for the specific office and communicate the title of the book,
- the {% role %}People Ops and Care/Facility{% endrole %} will buy the book, depending on availability of office budget.
- help to maintain the internal library. Please check what action is required for physical books on the [Library](Library.md) page.

In Agile Lab we have a digital library. 

If you want to buy a new digital book that is also DRM-free,
- follow the [purchasing process](#purchasing-process) below,
- help to maintain the internal library. Please check what action is required for digital books on the [Library](Library.md) page.



**Coffee**

Coffee is free !!!


# Expenses

This is mainly referred to work trip expenses.
Please arrange yourself the best solution. Don't ask People Operation to organize the travel for you, People Operation will only pay for it.
If you have time to plan a trip, do it in advance to reduce costs as you do with your personal trips.
You don't need authorizations unless you have to bypass soft limits, so don't ask for them.


**Car**

If you use your personal car for a work trip, the mileage ( KM ) is reimbursed according to local law. 
If you don't own a car and you think the best solution is to rent one, just do it.


**Launch/Dinner**

No limits. ( Spend company money like it is your own money )


**Hotel**

Soft limit at 100 Euro/Night. 
If you need to bypass the soft limit, ask the Buyer for an approval.


**Transportations**

Soft limit is 500 Euro overall for transportations.
If you need to bypass the soft limit, ask the Buyer for an approval.

**Conferences**

Soft limits described above are for generic work trips, not conferences. Conferences have different spending criteria, please refer to the [training budget benefit](Benefits.md#conferences) section.

# Purchasing process

## Scenario 1: self-purchase and reimbursement request

1. buy the good you need (obviously referring to the current guidelines for [Benefits](Benefits.md), and the other mentioned above)
2. add a new expense entry on Elapseit with:
    1. *type* : `07) RIMBORSO PIE' DI LISTA`
    2. *Project* :  it should depend on what the expense is related to. Imagine it's time instead of goods and use the same guidelines of projects as in [ProjectTimesheet](ProjectTimesheet.md). E.g. if it's personal training material, you must use `<current year> Agile - Training - Learning`.
    3. compile the other fields as usual (incurred date, actual currency, amount) and attach the receipt document
    4. submit the entry
3. the administration will include the reimbursement in the next payroll

## Scenario 2: request the Buyer to purchase something

1. find the good you need and verify if your personal budget allows this purchase (obviously referring to the current guidelines for [Benefits](Benefits.md), and the other mentioned above)
2. add a new expense entry on Elapseit with:
    1. *type* : `20) PURCHASED BY BUYER (RDA - RICHIESTA DI ACQUISTO)`
    2. *Project* :  it should depend on what the expense is related to. Imagine it's time instead of goods and use the same guidelines of projects as in [ProjectTimesheet](ProjectTimesheet.md). E.g. if it's personal training material, you must use `<current year> Agile - Training - Learning`.
    3. compile the other fields as usual (incurred date, actual currency, amount)
    4. submit the entry
    5. once submitted, get the Elapseit's _"Expense ID"_ (e.g. `ELI_0001234`)
3. email the Buyer of your choice, or buyer@agilelab.it, including:
    1. the link (and, if necessary, the instructions) for the purchase
    2. the Expense ID you got from point 2.5  (VERY IMPORTANT!!!)
4. if approved, the Buyer proceeds with the purchase and add by himself/herself the receipt document editing the Elapseit entry, leveraging the Expense ID to find it. This closes the process and the expense is trackable by everybody.

Following this process, you can easily track your personal training budget status, lead links can autonomously track the circle's expenses and administration processes will be more efficient.


