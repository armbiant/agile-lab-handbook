# Training

> *"The only thing worse than training your employees and having them leave is not training them and having them stay."*
> 
> **Henry Ford**

In AgileLab we believe that people's continuous learning and improvement is the only key to success.

For this reason, several [benefits](Benefits.md) are provided in order to incentive as much as possible the personal and professional growth. 

## The circle
AgileLab has a specific `circle:Engine/Training`, focused on engineers knowledge (technical and soft skills) improvement.
You are encouraged to get to know the circle and its offerings in terms of accountability and roles.

## Courses
AgileLab provides internal courses in its Learning Management System (LMS). For further details, please read the [Courses](Courses.md) page.

## Workshops
AgileLab frequently organizes internal tech workshops. For further details, please read the dedicated [section](Workshops.md).

## Language Learning
AgileLab provides the possibility to improve the language skills. For further details, please read the dedicated [Language Learning](LanguageLearning.md) page.

## Other Training material
You can find plenty of other training resources:
- Slides and past training material in `sharepoint://BigData/Documents/Training/`
- books: we have a [Library](Library.md) and a [Book club](BookClub.md)

Along with all these resources, as reported in [Benefits](Benefits.md), remember that you also have an important personal annual budget to spend in OTHER training material: make good use of it!

## Time Tracking

All the guidelines to be followed for time tracking on Elapseit are available in [Project Time Tracking](ProjectTimesheet.md) (focus on the Training section).
